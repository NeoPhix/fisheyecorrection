#include <iostream>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv/cv.h>
#include <opencv2/imgproc.hpp>

#define UNDISTORSION_IS_COMPLETED 0
#define ERROR_FILE_DOES_NOT_EXIST 1
#define ERROR_MISMATCHED_SIZES 2
#define ERROR_SAVE_IMAGE 3
#define ERROR_CORNERS_CANNOT_BE_FOUND 4

using namespace std;
using namespace cv;

bool classicCornersDetection(Mat &img, vector<Point2f> &corners, vector<Point3f> &board, const Size &boardSize);
bool cameraCalibration(const vector<Point2f> &corners, const vector<Point3f> board, const Mat &img, Mat &K, Mat &D);

int main(int argc, char *argv[])
{
	//Load image
	Mat original;
	Mat img;
	Size boardSize;	//Chessboard size (the inner corners)

	switch (argc)
	{
	case 1:
		original = imread("img2.png", CV_LOAD_IMAGE_UNCHANGED);
		boardSize = Size(10, 6);
		break;
	case 4:
	case 5:
		original = imread(argv[1], CV_LOAD_IMAGE_UNCHANGED);
		boardSize = Size( atoi(argv[2]), atoi(argv[3]) );
		break;
	default:
		cout << "Input arguemnts list is too long. You need filename as the first arguemnt;" << endl <<
				"the chessboard pattern size as the second and the third arguments" << endl;
		break;
	}
	if (original.empty())
	{
		cout << "Error! Image loading is failed. Please make sure that the input file exists" << endl;
		return ERROR_FILE_DOES_NOT_EXIST;
	}

	//resize(original, original, Size(original.cols / 2, original.rows / 2));

	cvtColor(original, img, CV_BGR2GRAY);

	//Find chessboard corners
	vector<Point2f> corners;
	vector<Point3f> board;			//Positions of real board corners (in 3D space)

	if ( !classicCornersDetection(img, corners, board, boardSize) )
	{
		cout << "Error! Corners of the chessboard cannot be detected" << endl;
		return ERROR_CORNERS_CANNOT_BE_FOUND;
	}

	drawChessboardCorners(img, boardSize, Mat(corners), true);
  	imshow("corners", img);
	waitKey(0);
	
	//Calibration
	Mat K = Mat::eye(3, 3, CV_64F);				//Camera parameters matrix
	Mat D = Mat::zeros(8, 1, CV_64F);			//Distorsion parameters
	
	if ( !cameraCalibration(corners, board, img, K, D) )
	{
		cout << "Error! Sizes of image points and object points arrays are mismatched or equals to 0" << endl;
		return ERROR_MISMATCHED_SIZES;
	}

	//Undistorsion
	Mat result;
	undistort(original, result, K, D);
	imshow("img", result);
	waitKey(0);

	//Save results
	bool saved = false;
	switch (argc)
	{
	case 5:
		cout << "The result of undistorison will be saved in " << argv[4] << endl;
		saved = imwrite(argv[4], result);
		break;
	default:
		cout << "The result of undistorison will be saved in result.png in default folder" << endl;
		saved = imwrite("result.png", result);
		break;
	}

	if (!saved)
	{
		cout << "Error! Image cannot be saved. Please make sure that the path exists" << endl;
		return ERROR_SAVE_IMAGE;
	}

	cout << "Undistorsion of input image is completed" << endl;
	return UNDISTORSION_IS_COMPLETED;
}


bool classicCornersDetection(Mat &img, vector<Point2f> &corners, vector<Point3f> &board, const Size &boardSize)
{
	for (int i = 0; i < boardSize.height; ++i)
	{
		for (int j = 0; j < boardSize.width; ++j)
		{
			board.push_back(Point3f(float(j), float(i), 0));
		}
	}
	
	bool found = findChessboardCorners( img, boardSize, corners,
										CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_NORMALIZE_IMAGE );
	if (found)
	{
		cornerSubPix( img, corners, Size(20, 20), Size(-1, -1),
					  TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1) );
	}
	else	//Trying to find right coefficients
	{
		//TO DO right matching of corners and board points. 
		Point2f zero2f(0, 0);
		Point3f zero3f(0, 0, 0);
		//Lambdas. Comparison between distances from 2 point to some zero point (the first iteration 0, 0 for finding left up corner of chessboard on image 
		auto distance2f = [zero2f](Point2f a, Point2f b)	
		{
			return norm(zero2f - a) > norm(zero2f - b);		//Comparison of distance between a and b to zero-point. 
		};
		auto distance3f = [zero3f](Point3f a, Point3f b)	
		{
			return norm(zero3f - a) > norm(zero3f - b);		
		};
		sort(corners.begin(), corners.end(), distance2f);
		sort(board.begin(), board.end(), distance3f);
		cout << "some" << endl;
		found = true;
	}
	return found;
}

bool cameraCalibration(const vector<Point2f> &corners, const vector<Point3f> board, const Mat &img, Mat &K, Mat &D)
{
	vector<vector<Point2f>> imagePoints;
	vector<vector<Point3f>> objectPoints;
	vector<Mat> rvecs, tvecs;

	imagePoints.push_back(corners);
	objectPoints.push_back(board);

	if (imagePoints[0].size() == objectPoints[0].size() && imagePoints[0].size() != 0)
	{
		double rms = calibrateCamera( objectPoints, imagePoints, img.size(), K, D, rvecs, tvecs,
									  CV_CALIB_FIX_K4 | CV_CALIB_FIX_K5 );
		return true;
	}
	return false;
}